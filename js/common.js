(function ($) {
    $('body').on('click', '.filter-param .filter__link', function () {
        $(this)
            .toggleClass('open')
            .parent()
            .find('.filter__dropdown')
            .toggleClass('hidden');
        return false;
    });

    $('body').on('click', '.filter-param a.filter-param__reset', function () {
        var inp = $(this).parent().find('input[type=reset]');
        inp.click();
        return false;
    });

    // $('.add-to-favorite').wrap('<div class="add-block">');

    $(window).resize(function () {
        if ($(this).width() > 752) {
            $('.filter, .filter-param').removeAttr('style');
            $('.catalog__filter_toggle').find('a').text('Показать фильтр');
        };
    });

    $('body').on('click', '.catalog__filter_toggle', function () {
        if ($(this).find('a').text() == 'Показать фильтр') {
            $(this).find('a').text('Скрыть фильтр');
        } else {
            $(this).find('a').text('Показать фильтр');
        }

        $('.filter, .filter-param')
            .slideToggle();
        return false;
    });

    $('.add-block_disabled').find('.add-to-favorite').text('Товар забронирован');  

    var check = $('#form1').find('#checkbox-fav');

    $('body').on('click', '.checkbox-stylized label', function () {
        check.attr('checked');
        $(this).toggleClass('check');
    });

    $('#form1').submit(function () {
        $(this).find('input, textarea').removeClass('input-error').removeAttr("placeholder");

        var form = $(this),
            btn = form.find('button[type=submit]'),
            inp = $(this).find('input[type=text]'),
            txt = $(this).find('#commentText');
        var mail = $(this).find('input.email');
        var check = $(this).find('#checkbox-fav');

        var size = 0;
        function checkInput() {
            form.find(inp).each(function () {
                if ($(this).val() != '') {
                    $(this).removeClass('input-error').removeAttr("placeholder");
                } else {
                    $(this).addClass('input-error').attr("placeholder", "Заполните поле");
                    size += 1;
                }
            });
            form.find(txt).each(function () {
                if ($(this).val() != '') {
                    $(this).removeClass('input-error').removeAttr("placeholder");
                } else {
                    $(this).addClass('input-error').attr("placeholder", "Заполните поле");
                    size += 1;
                }
            });
            form.find(check).each(function () {
                if ($(this).prop('checked') == true) {
                    $(this).parent().removeClass('input-error');
                    $(this).parent().find('label:before').removeClass('input-error');
                } else {
                    $(this).parent().addClass('input-error');
                    $(this).parent().find('label:before').addClass('input-error');
                    size += 1;
                }
            });
        };
        checkInput();
        if (size > 0) {
            return false;
        };
        $.post(
            $(this).attr('action'),
            $(this).serialize()
        );
        return false;
    });

    $('body').on('click', '.catalog-list__item > .remove', function () {
        $(this).parent().remove();
    });

})(jQuery);