$(function () {
    var popup = $('.popups-container');
    var list = $('.catalog__sort-by').find('.sort-dropdown');
    var addList = $('.catalog__sort-by').find('.add-list');
    var inpPopup = $('.new-list-popups').find('input');
    var addBlock = $('.catalog-list__item').find('.catalog__sort-by');
    var listArr = [];
    var id = 0;
    $('body').on('click', '.new-list-popups button[type=submit]', function () {
        $(this).parent().find('input').removeClass('error').removeAttr("placeholder");
        if (inpPopup.val().length == 0) {
            inpPopup.addClass("error").attr("placeholder", "Заполните поле");
            return false;
        } else {
            id++;
            var listName = inpPopup.val();
            var listItem = '<li class="sort-item"><span id="list' + id + '" class="list-item-name">' + listName + '</span><span></span></li>';
            addBlock.show();
            popup.hide();
            $('html').removeClass('popup__open');
            $(addList).before($(listItem));
            if (id < 2) {
                addBlock.find('.sort>span').text(listName);
                addBlock.find('.sort-dropdown>li:first').addClass('sort-item_active');
            }
            inpPopup.val('');
            return false;
        }
    });
    $('body').on('click', '.catalog-list__item .sort-item:not(.sort-item.add-list)', function () {
        $(this).addClass('sort-item_active')
            .siblings().removeClass('sort-item_active')
            .parent().parent().find('span:first').text($(this).text()).prop('class', $(this).attr('class'));
        added($(this).parents('.catalog-list__item').find('.add-to-favorite'));
        return false;
    });

    function added(el) {
        if ($(el).parent().find('.sort>span').hasClass('added') == true) {
            el.text('Добавлено в избранное').addClass('disabled');
        } else {
            el.text('Добавить в избранное').removeClass('disabled');
        }
    }
});

function count(el, event) {
    event.stopPropagation();
    console.log(el.length);
    if (el.length > 5) {
        $('.new-list-popups button[type=submit]').addClass('disabled');
        $('.new-list-popups input').addClass("error").attr("placeholder", "Вы превысили максимальное количество списков").prop('disabled', true);
    }
}

function addToList(el, event) {
    event.stopPropagation();
    var count = el.parent().find('.sort-item');

    if (count.length > 1) {
        el.parent().find('.sort>span').addClass('added');
        el.parent().find('.sort-dropdown .sort-item_active').addClass('added');
        el.text('Добавлено в избранное').addClass('disabled');
        el.stopPropagation();
    }
}