$(document).ready(function() {

   $("body").on("click", ".close__btn", function() {
        $(this).closest(".popups-container").hide();
        $(this).closest(".popup").hide();
        $("html").removeClass("popup__open");
        return false;
    });


    $("body").on("click", function(e) {
        if(!$(".popup").is(e.target) && $(".popup").has(e.target).length === 0) {
            $(".popups-container").hide();
            $(".popup").hide();
            $("html").removeClass("popup__open");
        };
    });

    $("body").on("submit", ".popup-form", function() {
        return validatePopupForm($(this));
    });
});

function validatePopupForm(form) {

    var regExp =  /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
    var valide = true;
    form.find("input").removeClass('error').removeAttr("placeholder");

    form.find("input[type='text'], input[type='password']").each(function() {
        if($(this).val().length == 0) {
            $(this).addClass("error").attr("placeholder", "Заполните поле");
            valide = false;
        }
    });

    form.find("input[data-type='email']").each(function() {
        if(!regExp.test($(this).val())) {
            $(this).addClass("error");
            valide = false;
        }
    });

   if (!valide) return false;
};

function showAuthorizationPopup(popup, event) {
    event.stopPropagation();
    $(".popups-container").show();
    popup.show();
    $("html").addClass("popup__open");
};
